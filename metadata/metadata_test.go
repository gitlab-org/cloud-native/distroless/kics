package metadata_test

import (
	"testing"

	"github.com/google/go-cmp/cmp"

	"gitlab.com/gitlab-org/security-products/analyzers/kics/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

func TestReportScanner(t *testing.T) {
	want := report.ScannerDetails{
		ID:      "kics",
		Name:    "kics",
		Version: metadata.ScannerVersion,
		Vendor: report.Vendor{
			Name: "GitLab",
		},
		URL: "https://github.com/Checkmarx/kics",
	}
	got := metadata.ReportScanner

	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("Wrong result. (-want +got):\n%s", diff)
	}
}
