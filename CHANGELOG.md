# KICS analyzer changelog

## v4.1.13
- update indirect dependencies (!100)

## v4.1.12
- upgrade [`Kics`](https://github.com/Checkmarx/kics) version [`1.7.12` => [`1.7.13`](https://github.com/Checkmarx/kics/releases/tag/v1.7.13)] (!99)
- upgrade `github.com/stretchr/testify` version [`v1.8.4` => [`v1.9.0`](https://github.com/stretchr/testify/releases/tag/v1.9.0)] (!99)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.7` => [`v2.0.8`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.8)] (!99)

## v4.1.11
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.6` => [`v2.0.7`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.7)] (!98)

## v4.1.10
- upgrade [`Kics`](https://github.com/Checkmarx/kics) version [`1.7.11` => [`1.7.12`](https://github.com/Checkmarx/kics/releases/tag/v1.7.12)] (!97)
- upgrade `github.com/urfave/cli/v2` version [`v2.26.0` => [`v2.27.1`](https://github.com/urfave/cli/releases/tag/v2.27.1)] (!97)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.3.1` => [`v4.3.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.2)] (!97)

## v4.1.9
- upgrade `github.com/google/go-cmp` version [`v0.5.9` => [`v0.6.0`](https://github.com/google/go-cmp/releases/tag/v0.6.0)] (!96)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.7` => [`v2.26.0`](https://github.com/urfave/cli/releases/tag/v2.26.0)] (!96)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.2.0` => [`v4.3.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.1)] (!96)

## v4.1.8
- upgrade [`Kics`](https://github.com/Checkmarx/kics) version [`1.7.9` => [`1.7.11`](https://github.com/Checkmarx/kics/releases/tag/v1.7.11)] (!95)

## v4.1.7
- upgrade [`Kics`](https://github.com/Checkmarx/kics) version [`1.7.7` => [`1.7.9`](https://github.com/Checkmarx/kics/releases/tag/v1.7.9)] (!93)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.5` => [`v4.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.2.0)] (!93)

## v4.1.6
- upgrade [`common`](gitlab.com/gitlab-org/security-products/analyzers/common/v3) to [`3.2.3`]((https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.3)) (!91)
  - Fix trusting Custom CA Certificate for UBI-based images
- Move custom CA bundle file path to trust anchors location in FIPS docker image (!91)

## v4.1.5
- upgrade [`Kics`](https://github.com/Checkmarx/kics) version [`1.7.5` => [`1.7.7`](https://github.com/Checkmarx/kics/releases/tag/v1.7.7)] (!90)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.4` => [`v2.0.6`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.6)] (!90)

## v4.1.4
- upgrade [`Kics`](https://github.com/Checkmarx/kics) version [`1.7.3` => [`1.7.5`](https://github.com/Checkmarx/kics/releases/tag/v1.7.5)] (!85)
    - Improvements to error handling for self references in JSON and YAML
    - Various bug fixes
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.3` => [`v4.1.5`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.5)] (!85)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.3` => [`v2.0.4`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.4)] (!85)

## v4.1.3
- upgrade [`Kics`](https://github.com/Checkmarx/kics) version [`1.7.0` => [`1.7.3`](https://github.com/Checkmarx/kics/releases/tag/v1.7.3)] (!79)
- upgrade `github.com/sirupsen/logrus` version [`v1.9.0` => [`v1.9.3`](https://github.com/sirupsen/logrus/releases/tag/v1.9.3)] (!79)
- upgrade `github.com/stretchr/testify` version [`v1.8.2` => [`v1.8.4`](https://github.com/stretchr/testify/releases/tag/v1.8.4)] (!79)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.3` => [`v2.25.7`](https://github.com/urfave/cli/releases/tag/v2.25.7)] (!79)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.1.0` => [`v2.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.2.0)] (!79)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.2` => [`v2.0.3`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.3)] (!79)

## v4.1.2
- Remap `Title` to `Name` field (!78)

## v4.1.1
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.2` => [`v4.1.3`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.3)] (!77)

## v4.1.0
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v1.4.1` => [`v2.0.2`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.2)] (!75)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.0` => [`v4.1.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.2)] (!75)

## v4.0.1
- upgrade [`Kics`](https://github.com/Checkmarx/kics) version [`1.6.13` => [`1.7.0`](https://github.com/Checkmarx/kics/releases/tag/v1.7.0)] (!74)
  - fix(query): serverless_function_without_unique_iam_role
  - enable security_group_rules_without_description on security_group_rule resources
- upgrade `github.com/urfave/cli/v2` version [`v2.25.1` => [`v2.25.3`](https://github.com/urfave/cli/releases/tag/v2.25.3)] (!74)

## v4.0.0
- Bump to next major version (!69)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v1.10.3` => [`v2.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.1.0)] (!69)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v3.22.1` => [`v4.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.0)] (!69)

## v3.7.11
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.2` => [`v1.10.3`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.3)] (!73)

## v3.7.10
- upgrade [`Kics`](https://github.com/Checkmarx/kics) version [`1.6.11` => [`1.6.13`](https://github.com/Checkmarx/kics/releases/tag/v1.6.13)] (!71)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.0` => [`v2.25.1`](https://github.com/urfave/cli/releases/tag/v2.25.1)] (!71)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.1` => [`v1.10.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.2)] (!71)

## v3.7.9
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report` version [`v3.17.0` => [`v3.22.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.22.1)] (!70)

## v3.7.8
- upgrade [`Kics`](https://github.com/Checkmarx/kics) version [`1.6.10` => [`1.6.11`](https://github.com/Checkmarx/kics/releases/tag/v1.6.11)] (!68)
  - Adds one new rule and improvements to two rules.
  - Various bug fixes and dependency updates.
- upgrade `github.com/stretchr/testify` version [`v1.8.1` => [`v1.8.2`](https://github.com/stretchr/testify/releases/tag/v1.8.2)] (!68)
- upgrade `github.com/urfave/cli/v2` version [`v2.24.3` => [`v2.25.0`](https://github.com/urfave/cli/releases/tag/v2.25.0)] (!68)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset` version [`v1.4.0` => [`v1.4.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v1.4.1)] (!68)
- Update Go to 1.19 (!68)

## v3.7.7
- upgrade [`Kics`](https://github.com/Checkmarx/kics) version [`1.6.7` => [`1.6.10`](https://github.com/Checkmarx/kics/releases/tag/v1.6.10)] (!66)
  - Adds several new queries and bug fixes.
- upgrade `github.com/urfave/cli/v2` version [`v2.23.7` => [`v2.24.3`](https://github.com/urfave/cli/releases/tag/v2.24.3)] (!66)

## v3.7.6
- upgrade [`Kics`](https://github.com/Checkmarx/kics) version [`1.6.6` => [`1.6.7`](https://github.com/Checkmarx/kics/releases/tag/v1.6.7)] (!65)
  - Adds two new rules and refactors some analysis logic.
  - General bug fixes, dependency, and documentation updates.
- upgrade `github.com/urfave/cli/v2` version [`v2.23.6` => [`v2.23.7`](https://github.com/urfave/cli/releases/tag/v2.23.7)] (!65)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.0` => [`v1.10.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.1)] (!65)

## v3.7.5
- upgrade [`Kics`](https://github.com/Checkmarx/kics) version [`v1.6.5` => [`v1.6.6`](https://github.com/Checkmarx/kics/releases/tag/v1.6.6)] (!64)
  - Improves some existing queries.
- upgrade `github.com/urfave/cli/v2` version [`v2.23.5` => [`v2.23.6`](https://github.com/urfave/cli/releases/tag/v2.23.6)] (!64)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.16.0` => [`v3.17.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.17.0)] (!64)

## v3.7.4
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.16.0` => [`v3.17.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.17.0)] (!62)
  - Sorts vulnerabilities in the generated report.
- Replace downstream tests with `integration-test`

## v3.7.3
- Ensure `git` is installed in the FIPS image (!63)

## v3.7.2
- correctly log output from the `kics` binary when setting `SECURE_LOG_LEVEL=debug` (!61)

## v3.7.1
- upgrade [`Kics`](https://github.com/Checkmarx/kics) version [`v1.6.2` => [`v1.6.5`](https://github.com/Checkmarx/kics/releases/tag/v1.6.5)] (!59)
  - Adds a "Vulnerable OpenSSL Version" query for Dockerfile.
  - Improves the performance of some existing queries.
  - Fixes an issue where some Terraform configurations will cause the scanner to panic.
- upgrade `github.com/stretchr/testify` version [`v1.8.0` => [`v1.8.1`](https://github.com/stretchr/testify/releases/tag/v1.8.1)] (!59)
- upgrade `github.com/urfave/cli/v2` version [`v2.19.2` => [`v2.23.5`](https://github.com/urfave/cli/releases/tag/v2.23.5)] (!59)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.2` => [`v1.10.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.0)] (!59)

## v3.7.0
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.15.5` => [`v3.16.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.16.0)] (!58)
    - Fixes an issue where INFO-level findings are incorrectly mapped to the UNKNOWN severity level.

## v3.6.0
- Populates the `cve` field of each vulnerability finding (!57)

## v3.5.0
- upgrade [`Kics`](https://github.com/Checkmarx/kics) version [`1.6.0` => [`1.6.2`](https://github.com/Checkmarx/kics/releases/tag/v1.6.2)] (!55)
  - Improves several rules, including addressing false positive detections. You may see less findings as a result.
  - Modifies the severity levels of some rules.
  - Adds new queries for Cloudformation and Terraform.
- upgrade `github.com/urfave/cli/v2` version [`v2.16.3` => [`v2.19.2`](https://github.com/urfave/cli/releases/tag/v2.19.2)] (!55)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.1` => [`v1.9.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.2)] (!55)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.1` => [`v3.2.2`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.2)] (!55)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.14.0` => [`v3.15.5`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.15.2)] (!55)
  - Produce reports adhering to version 14.0.4 of the Security Report schema.

## v3.4.0
- Change FIPS base image to ubi8-minimal (!56)

## v3.3.2
- Bump go-fips builder image to 1.18 (!54)

## v3.3.1
- Update common to `v3.2.1` to fix gotestsum cmd (!53)

## v3.3.0
- upgrade [`Kics`](https://github.com/Checkmarx/kics) version [`1.5.13` => [`1.6.0`](https://github.com/Checkmarx/kics/releases/tag/v1.6.0)] (!51)
- upgrade `github.com/google/go-cmp` version [`v0.5.8` => [`v0.5.9`](https://github.com/google/go-cmp/releases/tag/v0.5.9)] (!51)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.1` => [`v2.16.3`](https://github.com/urfave/cli/releases/tag/v2.16.3)] (!51)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.12.2` => [`v3.14.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.14.0)] (!51)

## v3.2.0
- upgrade [`Kics`](https://github.com/Checkmarx/kics) version [`1.5.12` => [`1.5.13`](https://github.com/Checkmarx/kics/releases/tag/v1.5.13)] (!49)
- upgrade `github.com/google/go-cmp` version [`v0.5.6` => [`v0.5.8`](https://github.com/google/go-cmp/releases/tag/v0.5.8)] (!49)
- upgrade `github.com/sirupsen/logrus` version [`v1.8.1` => [`v1.9.0`](https://github.com/sirupsen/logrus/releases/tag/v1.9.0)] (!49)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.0` => [`v2.11.1`](https://github.com/urfave/cli/releases/tag/v2.11.1)] (!49)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.8.2` => [`v1.9.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.1)] (!49)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.12.2` => [`v3.13.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.13.0)] (!49)

## v3.1.3
- Change `log.Fatal(...)` to `log.Error(...)` and return the error when the scanner cmd execution fails (!46)
- Separate error and debug buffers so that when a cmd execution error occurs, it logs only the error buffer under `log.Error` (!46)

## v3.1.2
- Upgrade the `command` package for better analyzer messages (!48)

## v3.1.1
- Update kics to [1.5.12](https://github.com/Checkmarx/kics/releases/tag/v1.5.12) (!44)
  + Notable changes:
    + feat(query): add new k8s rule to detect attach permission (RBAC)
    + feat(query): add query to check iam policy to invoke lambda
    + fix(scan behavior): ignore broken symlink (addresses GL issue #359970)
  + Fix/update existing queries
  + Fix bugs in scanning and analysis
  + Fix CVE-2022-1586 and CVE-2022-29810

## v3.1.0
- Upgrade core analyzer dependencies (!45)
  + Adds support for globstar patterns when excluding paths
  + Adds analyzer details to the scan report

## v3.0.0
- Disable kics secret detection (!43)
- Add git to Docker image (!43)

## v2.0.3
- Improve exit status handling (!37)

## v2.0.2
- Update kics to [1.5.10](https://github.com/Checkmarx/kics/releases/tag/v1.5.10) (!41)
    + Add 82 new queries (rules) for Ansible, CloudFormation, Docker Compose, Kubernetes, and Terraform
    + Fix/update existing queries
    + Fix bugs in scanning and analysis
    + Improve performance
    + 1.5.10 [release notes](https://github.com/Checkmarx/kics/releases/tag/v1.5.10)
    + 1.5.9 [release notes](https://github.com/Checkmarx/kics/releases/tag/v1.5.9)
    + 1.5.8 [release notes](https://github.com/Checkmarx/kics/releases/tag/v1.5.8)
    + 1.5.7 [release notes](https://github.com/Checkmarx/kics/releases/tag/v1.5.7)
    + 1.5.6 [release notes](https://github.com/Checkmarx/kics/releases/tag/v1.5.6)

## v2.0.1
- Fix kics exiting with SECURE_LOG_LEVEL=warn (!40)

## v2.0.0
- Bump to next major version, 2.0.0 (!36)

## v1.5.2
- Add custom CA support for FIPS docker image (!34)

## v1.5.1
- Add `Dockerfile.fips` to include FIPS image for this release and subsequent releases (!30)

## v1.5.0
- Update kics to [1.5.5](https://github.com/Checkmarx/kics/releases/tag/v1.5.5) (!31)
    + 1.5.5 Notable Changes
      + fix(query): updated ebs not optimized queries
      + fix(query): defined NET_BIND_SERVICE as exception in containers_with_added_capabilities k8s
      + fix(query): extended containers_running_as_root k8s rule to work if no securityContext is defined
      + fix(query): refined missing_app_armor_config k8s rule to operate on specific containers
      + fix(query): fixed "S3 Static Website Host Enabled" for CF
      + fix(query): update validCertificate.pem for "Certificate Has Expired" query
      + fix(query): fixed Client Certificate Authentication Not Setup Properly
      + delete(query): removed query lambda_function_without_tags
      + delete(query): removed redundant default_service_account_in_use k8s rule
      + delete(query): removed redundant resource_with_allow_privilege_escalation k8s rule
      + update(common lib): improved performance of get_nested_values_info
      + update(query): updated AWS IAM Policy Grants Full Permissions for Terraform
    + 1.5.4 Notable Changes
      + feat(analyzer & parser): decrypt Ansible Vault file on the fly in
      + fix(query): revise list of unsafe sysctls in cluster_allows_unsafe_sysctls k8s rule
      + fix(query): fix searchKey and additional resource kinds in volume_mount_with_os_directory_write_permissions k8s rule
      + fix(query): extend image_without_digest k8s rule to cover further resource kinds
      + fix(query): extend container_requests_not_equal_to_its_limits k8s rule to cover further resource kinds and remove redundant checks
      + fix(query): extend image_pull_policy_of_container_is_not_always k8s rule to cover additional resource kinds
      + fix(query): extend net_raw_capabilities_not_being_dropped k8s rule to cover further resource kinds
      + fix(query): k8s rule service_account_token_automount_not_disabled should also consider automount option in ServiceAccount
      + fix(query): add a missing SSE way for SQS
      + fix(query): show privilege_escalation_allowed k8s alert also in case no securityContext is defined
      + fix(query): extend memory_limits_not_defined k8s rule to cover further resource kinds
      + fix(query): extend memory_requests_not_defined k8s rule to cover further resource kinds
      + fix(query): fix path to spec in root_container_not_mounted_as_read_only k8s rule
      + fix(query): S3 Bucket Policy Accepts Http Requests

## v1.4.0
- Update kics to [1.5.3](https://github.com/Checkmarx/kics/releases/tag/v1.5.3) (!29)
    + 1.5.3 Notable Changes
      + fix(analyzer): fixed and improved regexes in analyzer
      + fix(query): extend container_is_privileged k8s rule to cover additional resource kinds
      + fix(library): fixed "Generic:354: rego_type_error: rule named engines redeclared at Common:354"
      + fix(query): corrected tag flagging invalid_image k8s rule
      + refactor(query): simplify docker_daemon_socket_is_exposed_to_containers k8s rule
    + 1.5.2 Notable Changes
      + feat(terraform): added support for better IAM Policy evaluation and basic terraform resource relationship querying
      + fix(query): fixed vulnerable policies queries
      + fix(query): be able to check default_tags on multiple providers
      + fix(query): fixed "Service Control Policies Disabled" query

## v1.3.1
- Update `report` module to exclude semgrep sarif suppressions (!28)

## v1.3.0
- Update ruleset, report, and command modules to support ruleset overrides (!25)

## v1.2.2
- Update go to v1.17 (!24)

## v1.2.1
- Disables network requests for full descriptions and crash reports (!23)

## v1.2.0
- Update kics to [1.5.1](https://github.com/Checkmarx/kics/releases/tag/v1.5.1) (!21)
    + 1.5.1 Notable Changes
      + feat(analyzer): added support to Cloud Development Kit for Terraform (CDKTF)
      + feat(buildah): added initial Buildah support
      + fix(query): fix terraform query for ingress/egress description
      + fix(golang): fixed golang data races and make file
      + fix(version): fixed bug with version checking
      + fix(parser): added type handler to Terraform convertBody function
      + fix(parser): added YAML alias as string
      + fix(query): limited "IAM Access Analyzer Undefined" only for AWS
      + fix(query): service should match containerPort using targetPort
      + fix(report): fixed CycloneDX report for compressed files
      + fix(report): fixed null ASFF report
      + build(deps): bump github.com/hashicorp/hcl/v2 from 2.10.1 to 2.11.1
      + build(deps): bump github.com/spf13/cobra from 1.2.1 to 1.3.0
      + build(deps): bump github.com/BurntSushi/toml from 0.4.1 to 1.0.0
      + build(deps): bump github.com/aws/aws-sdk-go from 1.37.0 to 1.42.44
      + build(deps): bump github.com/johnfercher/maroto from 0.33.0 to 0.34.0
      + build(deps): bump helm.sh/helm/v3 from 3.7.2 to 3.8.0
      + build(deps): bump github.com/hashicorp/go-getter from 1.5.9 to 1.5.11
      + build(deps): bump github.com/tdewolff/minify/v2 from 2.9.29 to 2.10.0
      + build(deps): bump github.com/emicklei/proto from 1.9.1 to 1.9.2
      + build(deps): bump github.com/open-policy-agent/opa from 0.34.2 to 0.37.1
      + ci(deps): bump peter-evans/create-pull-request from 3.12.0 to 3.12.1
      + ci(deps): bump docker/build-push-action from 2.8.0 to 2.9.0
      + update(report): updated gitlab sast report schema version
      + update(terraformer): added timestamp to generated import folder
      + build(env): added dev build tag
      + docs(kics.io): removed references to binaries usage and changed all cmds to Docker cmds
    + 1.5.0 Notable Changes
      + feat(terraformer): added terraformer integration
      + feat(SAM): added support to AWS Serverless Application Model
      + feat(report): added ASFF report
      + feat(parser): support of YAML alias
      + fix(yaml): ignore lines by comments
      + fix(core): Fixed bug when trying to read encrypted zip file
      + fix(parser): fixed KICS panic in getLastElementLine
      + fix(detector): fixed KICS panic in getKeyWithCurlyBrackets
      + fix(parser): fixed KICS panic in empty fifo value access
      + fix: deleted extraction folder after KICS scan
      + fix(query): deleting searchLine in "Resource Not Using Tags" for Terraform
      + fix(query): updated "S3 Bucket Without Enabled MFA Delete" for Terraform
      + fix(query): updated "CloudFront Without Minimum Protocol TLS 1.2" for Ansible, CloudFormation, and Terraform
      + fix(query): refactored "DB Security Group Has Public IP" for Ansible, CloudFormation, and Terraform
      + feat(queries): update terraform registry data on commons.json
    + 1.4.9 Notable Changes
      + feat(gdm): added support to Google Deployment Manager
      + feat(grpc): added support to gRPC
      + fix(query): fixed query Multiple RUN, ADD, COPY, Instructions Listed
      + fix(query): "Azure Container Registry With No Locks" for Ansible
      + fix(core): fixed negative lines and terminal checking
      + fix(logs): fixed log error messages polution
      + feat(query): add allow rule for ansible-vault
      + refactor(query): policies for CloudFormation
      + docs(queries): all query csv file downloads now come with the name kics-queries.csv
    + 1.4.8 Notable Changes
      + fix(scan): not reporting error when progress bar fails to close
      + fix(parser): fixed YAML parser panic with wrong type for interface
    + 1.4.7 Notable Changes
      + feat(engine): added data source policy to terraform
      + feat(parser): enabled parsers ignore comment by line
      + refactor(query): updated query Chown Flag Exists description
      + fix(race): fix kics Golang data races
      + fix(detector): fix panic with interpolated brackets in detector
      + fix(source): fixed KICS panic when reading invalid metadata
      + fix(report): fixed bug with invalid startLine on sarif report
    + 1.4.6 Notable Changes
      + feat(parser): removed resources with count set to 0 in payload
      + feat(kics): add version checking
      + fix(query): correcting severity and category for 'Default Azure Storage Account Network Access Is Too Permissive'
      + refactor(report): if no files to scan are found kics will no longer create report files
      + fix(helm): fixed helm only excluding template files
      + fix(inspector): KICS panicking when using KICS repo with -q flag
      + fix(parser): parsers now stringify the original content in a formatted way

## v1.1.0
- Update ruleset module to include rule pack synthesis (!20)

## v1.0.2
- Improve error message when a system problem occurs (!17 @rndmh3ro)

## v1.0.1
- Bump `analyzers/report` to latest v3.7.1 to suppress misleading SARIF warning (!16)

## v1.0.0
- Promote kics to v1 (!15)
- Update report version to v3.7.0 (!15)
  - Map sarif rule name to identifier.name

## v0.1.0
- Propagate loglevel to kics (!8)

## v0.0.4
- Move sarif package to the report dependency (!5)

## v0.0.3
- Lowercase scanner id to be kics (!6)

## v0.0.2
- Switch to KICS (!1)

## v0.0.1
- init terrscan analyzer
