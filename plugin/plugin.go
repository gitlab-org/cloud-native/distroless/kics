package plugin

import (
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/plugin"
)

// Match checks if the filename has a .tf extension
func Match(path string, info os.FileInfo) (bool, error) {
	ext := filepath.Ext(info.Name())

	if matchTerraform(ext) {
		return true, nil
	}

	if matchCloudFormation(ext) {
		return true, nil
	}

	return false, nil
}

func matchTerraform(ext string) bool {
	if ext == ".tf" {
		return true
	}

	return false
}

// https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-whatis-concepts.html#cfn-concepts-templates
func matchCloudFormation(ext string) bool {
	switch ext {
	case
		".json",
		".yaml",
		".yml",
		".template",
		".txt":
		return true
	}

	return false
}

func init() {
	plugin.Register("kics", Match)
}
