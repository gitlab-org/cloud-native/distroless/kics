ARG SCANNER_VERSION=v1.7.13

FROM checkmarx/kics:${SCANNER_VERSION}-alpine as kics
FROM golang:1.19-alpine AS build

ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .

# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer

FROM alpine:latest

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-v1.7.13}
# Disable crash reporting by default https://docs.kics.io/latest/commands/#disable_crash_report
ENV DISABLE_CRASH_REPORT=0

RUN apk update && \
    apk add --no-cache git && \
    apk upgrade

COPY --from=kics --chown=root:root /app/bin/assets /usr/local/bin/assets
COPY --from=kics --chown=root:root /app/bin/kics /usr/local/bin/kics
COPY --from=build --chown=root:root /go/src/app/analyzer /

ENTRYPOINT []
CMD ["/analyzer", "run"]
