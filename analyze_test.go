package main

import (
	"testing"

	log "github.com/sirupsen/logrus"

	"github.com/google/go-cmp/cmp"
)

func TestBuildArgs(t *testing.T) {
	tests := []struct {
		projPath string
		logLevel log.Level
		want     []string
	}{
		{
			"/foo/bar",
			log.InfoLevel,
			[]string{
				"scan", "--ci",
				"--path", "/foo/bar",
				"--queries-path", defaultKicsQueryPath,
				"--disable-full-descriptions",
				"--disable-secrets",
				"--log-level", "INFO",
				"--output-path", "/tmp",
				"--output-name", "kics",
				"--report-formats", "sarif",
			},
		},
		{
			"/foo/bar",
			log.DebugLevel,
			[]string{
				"scan", "--ci",
				"--path", "/foo/bar",
				"--queries-path", defaultKicsQueryPath,
				"--disable-full-descriptions",
				"--disable-secrets",
				"--log-level", "DEBUG",
				"--output-path", "/tmp",
				"--output-name", "kics",
				"--report-formats", "sarif",
			},
		},
		{
			"/foo/bar",
			log.WarnLevel,
			[]string{
				"scan", "--ci",
				"--path", "/foo/bar",
				"--queries-path", defaultKicsQueryPath,
				"--disable-full-descriptions",
				"--disable-secrets",
				"--log-level", "WARN",
				"--output-path", "/tmp",
				"--output-name", "kics",
				"--report-formats", "sarif",
			},
		},
	}

	for idx := range tests {
		test := tests[idx]

		log.SetLevel(test.logLevel)

		got := buildArgs(test.projPath, defaultKicsQueryPath)
		if diff := cmp.Diff(test.want, got); diff != "" {
			t.Errorf("Report mismatch (-want +got):\n%s", diff)
		}
	}
}
